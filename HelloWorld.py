# Zadania domowe

# Zadanie 1
def powitanie():
    imie=input("Podaj swoje imie")
    if imie=="Magda":
        print("Czesc "+imie+"! Ladna dzis pogoda!")
    else:
        print("Czesc "+imie+"!")

# Zadanie 2 ,,
# Tu wprowadziłam zmianę w branchu master
# Tu wprowadziłam zmianę w Branch1
def factorial(liczba):
    try:
        if liczba==0:
            print("Wynik to 0")
        else:
            c=1
            wynik=1
            while c<=liczba:
                wynik=wynik*c
                c=c+1
            print("Wynik to "+str(wynik))
    except:
        print("Podano nieprawidlowa wartosc. Podaj liczbe calkowita.")


# Zadanie 3
# czesc 1
def is_leap(rok):
    if(rok%4==0 and rok%100!=0 or rok%400==0):
        return(True)
    else:
        return(False)

# czesc 2
def leap_years(n):
    leap_years=[]
    year=2019
    while len(leap_years)<n:
        if is_leap(year)==True:
            leap_years.append(year)
        year+=1
    print(leap_years)
